import React from 'react';
import { Links } from "./Links.js";
import { IntlProvider, FormattedNumber, FormattedDate, FormattedRelative, FormattedMessage } from 'react-intl';

class App extends React.Component {
     constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.state = {
            locale: 'pl'
        }
    }

    render() {
        const locales = this.props.locales;
        const { locale } = this.state;
        return (            
            <IntlProvider locale={locale} messages={this.props.messages[locale]} >
                <div>
                    <Links locales={locales} handleChange={ this.handleChange } />
                    <br />
                    <div>
                        <FormattedNumber value={42000} />
                    </div>
                    <br />
                    <div>
                        <FormattedNumber
                            value={99.95}
                            style="currency"
                            currency="PLN" />
                    </div>
                    <br />
                    <div>
                        <FormattedDate
                            value={new Date()}
                            year='numeric'
                            month='long'
                            day='numeric'
                            weekday='long'
                        />
                    </div>
                    <br />
                    <div>
                        <FormattedRelative 
                            value={new Date()} 
                            updateInterval={500}/>
                    </div>
                    <br />
                    <div>
                        <FormattedMessage
                            id='greeting'
                            values={{
                            name: 'Peter Parker'
                            }} />
                    </div>
                </div>
            </IntlProvider>
        )
    }    

    handleChange(locale) {
        this.setState({locale: locale});
    }

}

export default App;