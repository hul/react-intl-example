import React from 'react';
import ReactDOM from 'react-dom';
import pl from 'react-intl/locale-data/pl';
import fr from 'react-intl/locale-data/fr';
import es from 'react-intl/locale-data/es';
import en from 'react-intl/locale-data/en';
import {addLocaleData} from 'react-intl';
import enMessages from '../l10n/en.json';
import plMessages from '../l10n/pl.json';
import frMessages from '../l10n/fr.json';
import esMessages from '../l10n/es.json';

import App from './App';

addLocaleData([...es, ...pl, ...fr, ...en]);

const locales = ['pl', 'fr', 'es', 'en'];
const messages = {
    "en": enMessages, 
    "pl": plMessages, 
    "fr": frMessages,
    "es": esMessages
}

ReactDOM.render(
    <App locales={locales} messages={messages}/>,
    document.getElementById('root')
);