import React from 'react';

const Links = ({locales, handleChange}) => {
    return (
        <div>
            {locales.map((locale) => {
                const onClick = () => { handleChange(locale) }
                return (
                    <a style={{marginRight: 5}} href="#" key={locale} onClick={ onClick } >
                        {locale}
                    </a>
                )
            })}
        </div>        
    )
}

export { Links }
